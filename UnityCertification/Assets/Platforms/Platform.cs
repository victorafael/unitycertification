﻿using UnityEngine;

public class Platform : MonoBehaviour
{
    private const float SlipperyEdgeSize = 0.1f;
    private static PhysicsMaterial2D slipperyEdgeMaterial;
	void Awake()
	{
        this.tag = GameplayConstants.TAG_Ground;    // If you get an error here, create a Tag in Unity called "Ground".
        //See the GameplayConstants.cs file for other required Tags and Layers.
        
        SpriteRenderer spriteRenderer = this.GetComponent<SpriteRenderer> ();
		if (spriteRenderer != null)
		{
            MatchColliderToSpriteSize(spriteRenderer);
		}
	}

    public void MatchCollider()
    {
        SpriteRenderer spriteRenderer = this.GetComponent<SpriteRenderer>();
        if (spriteRenderer != null)
        {
            MatchColliderToSpriteSize(spriteRenderer, false);
        }
    }
    private void MatchColliderToSpriteSize(SpriteRenderer spriteRenderer, bool doSlipperySides = true)
    {
        if(slipperyEdgeMaterial == null)
        {
            slipperyEdgeMaterial = new PhysicsMaterial2D("slippery");
            slipperyEdgeMaterial.bounciness = 0;
            slipperyEdgeMaterial.friction = 0;
        }
        BoxCollider2D coll = this.GetComponent<BoxCollider2D>();

        if (coll == null)
        {
            coll = this.gameObject.AddComponent(typeof(BoxCollider2D)) as BoxCollider2D;
        }

        Vector2 size = spriteRenderer.size;
        size.x -= SlipperyEdgeSize * 2;

        coll.size = size;
        
        coll.offset = 0.5f * spriteRenderer.size.y * Vector2.up;


        if (!doSlipperySides) return;

        Vector2 slipperyColSize = new Vector2(SlipperyEdgeSize, size.y);
        Vector2 slipperyOffset = 0.5f * spriteRenderer.size.y * Vector2.up + Vector2.right * (spriteRenderer.size.x * .5f - SlipperyEdgeSize * 0.5f);

        coll = gameObject.AddComponent<BoxCollider2D>();
        coll.size = slipperyColSize;
        coll.offset = slipperyOffset;
        coll.sharedMaterial = slipperyEdgeMaterial;

        slipperyOffset.x *= -1;

        coll = gameObject.AddComponent<BoxCollider2D>();
        coll.size = slipperyColSize;
        coll.offset = slipperyOffset;
        coll.sharedMaterial = slipperyEdgeMaterial;
    }
}
