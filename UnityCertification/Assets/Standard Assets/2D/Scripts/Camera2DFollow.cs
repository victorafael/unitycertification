using System;
using NUnit.Framework.Constraints;
using UnityEngine;

namespace UnityStandardAssets._2D
{
    public class Camera2DFollow : MonoBehaviour
    {
        private Camera cam;
        public Transform target;
        public float damping = 1;
        public float lookAheadFactor = 3;
        public float lookAheadReturnSpeed = 0.5f;
        public float lookAheadMoveThreshold = 0.1f;
        public float minimumHeight = -10f;

        private float m_OffsetZ;
        private Vector3 m_LastTargetPosition;
        private Vector3 m_CurrentVelocity;
        private Vector3 m_LookAheadPos;
        private float minimumDistance;

        public BoxCollider2D blockPlayerMovement;
        public BoxCollider2D enemyKiller;

        // Use this for initialization
        private void Start()
        {
            m_LastTargetPosition = target.position;
            m_OffsetZ = (transform.position - target.position).z;
            transform.parent = null;
            
            
            cam = GetComponent<Camera>();

            Vector3 pos = cam.ViewportToWorldPoint(new Vector3(0, 0.5f, 10));
            blockPlayerMovement.offset = new Vector2(pos.x + blockPlayerMovement.size.x * 0.5f, 0);
            enemyKiller.offset = new Vector2(pos.x - enemyKiller.size.x * 0.5f, 0);
        }


        // Update is called once per frame
        private void Update()
        {
            minimumDistance = Mathf.Max(transform.position.x, minimumDistance);
            // only update lookahead pos if accelerating or changed direction
            float xMoveDelta = (target.position - m_LastTargetPosition).x;

            bool updateLookAheadTarget = Mathf.Abs(xMoveDelta) > lookAheadMoveThreshold;

            if (updateLookAheadTarget)
            {
                m_LookAheadPos = lookAheadFactor*Vector3.right*Mathf.Sign(xMoveDelta);
            }
            else
            {
                m_LookAheadPos = Vector3.MoveTowards(m_LookAheadPos, Vector3.zero, Time.deltaTime*lookAheadReturnSpeed);
            }

            Vector3 aheadTargetPos = target.position + m_LookAheadPos + Vector3.forward*m_OffsetZ;
            Vector3 newPos = Vector3.SmoothDamp(transform.position, aheadTargetPos, ref m_CurrentVelocity, damping);
            newPos.x = Mathf.Max(newPos.x, minimumDistance);
            newPos.y = Mathf.Max(newPos.y, minimumHeight);

            transform.position = newPos;

            m_LastTargetPosition = target.position;
        }
    }
}
