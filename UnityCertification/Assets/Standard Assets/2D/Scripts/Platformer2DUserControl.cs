using System;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

namespace UnityStandardAssets._2D
{
    [RequireComponent(typeof (PlatformerCharacter2D))]
    public class Platformer2DUserControl : MonoBehaviour
    {
        private PlatformerCharacter2D m_Character;
        private bool m_Jump;

        public bool autoControl;
        public Transform frontPosition;
        public Transform centerPosition;
        public Transform backPosition;
        public float holeMinDistance;
        public float obstacleDistance;
        public float landingPositionDistance;
        public float enemyLandingTolerance = 0.1f;
        public float maxEnemySqrDistance = 21;

        public const int GroundLayer = 0;
        public const int EnemyLayer = 9;

        private int groundAndEnemies;

        private bool canLand;
        private float lastJump;
        private Transform targetEnemy;

        private void Awake()
        {
            m_Character = GetComponent<PlatformerCharacter2D>();

            groundAndEnemies = 1 << GroundLayer | 1 << EnemyLayer;
        }


        private void Update()
        {
            if (!m_Jump && !autoControl)
            {
                // Read the jump input in Update so button presses aren't missed.
                m_Jump = CrossPlatformInputManager.GetButtonDown("Jump");
            }
        }


        private void FixedUpdate()
        {
            if (autoControl)
            {
                DoAutoControl();
                return;
            }
            // Read the inputs.
            bool crouch = Input.GetKey(KeyCode.LeftControl);
            float h = CrossPlatformInputManager.GetAxis("Horizontal");
            // Pass all parameters to the character control script.
            m_Character.Move(h, crouch, m_Jump);
            m_Jump = false;
        }

        
        void DoAutoControl()
        {
            float move = 0;
            bool jump = false;
            if (m_Character.IsGrounded)
            {
                move = 1;
                RaycastHit2D down = Physics2D.Raycast(frontPosition.position, Vector2.down, holeMinDistance, groundAndEnemies);
                RaycastHit2D forward = Physics2D.Raycast(frontPosition.position, Vector2.right, obstacleDistance, groundAndEnemies);
                if(down.collider == null || (forward.collider != null && !forward.collider.isTrigger))
                {
                    if(lastJump <= Time.time - 0.2f) { 
                        jump = true;
                        Debug.DrawLine(transform.position, transform.position + Vector3.up * 100, Color.yellow, 50);
                        lastJump = Time.time;
                        canLand = false;
                    }
                }
            }
            else
            {
                move = 1;
                jump = false;

                if(targetEnemy != null && targetEnemy.gameObject.activeInHierarchy && Vector3.SqrMagnitude(transform.position - targetEnemy.position) < maxEnemySqrDistance)
                {
                    if(Mathf.Abs(targetEnemy.position.x - transform.position.x) > enemyLandingTolerance)
                    {
                        move = Mathf.Sign(targetEnemy.position.x - transform.position.x);
                    }
                    else
                    {
                        move = 0;
                    }
                }else if (!canLand && !m_Character.IsFalling) {
                    targetEnemy = null;
                    RaycastHit2D down = Physics2D.Raycast(centerPosition.position, Vector2.down, landingPositionDistance, groundAndEnemies);
                    if(down.collider == null || down.collider.isTrigger)
                    {
                        canLand = true;
                    }
                    else if(down.collider.gameObject.layer == EnemyLayer)
                    {
                        targetEnemy = down.collider.transform;
                    }
                }
                else
                {
                    targetEnemy = null;
                    RaycastHit2D down = Physics2D.Raycast(backPosition.position, Vector2.down, landingPositionDistance, groundAndEnemies);
                    if (down.collider != null && !down.collider.isTrigger)
                    {
                        move = 0;
                        if (down.collider.gameObject.layer == EnemyLayer)
                        {
                            targetEnemy = down.collider.transform;
                        }
                    }
                    
                }
            }
            m_Character.Move(move, false, jump);
        }


        private void OnDrawGizmos()
        {
            if (frontPosition != null && backPosition != null && centerPosition != null)
            {
                Gizmos.DrawLine(frontPosition.position, frontPosition.position + Vector3.down * holeMinDistance);
                Gizmos.DrawLine(frontPosition.position, frontPosition.position + Vector3.right * obstacleDistance);
                Gizmos.DrawLine(backPosition.position, backPosition.position + Vector3.down * landingPositionDistance);
            }
            if(Application.isPlaying && autoControl)
            {
                Gizmos.color = m_Character.IsGrounded ? Color.green : Color.red;
                Gizmos.DrawSphere(centerPosition.position, 0.2f);
            }
        }
    }
}
