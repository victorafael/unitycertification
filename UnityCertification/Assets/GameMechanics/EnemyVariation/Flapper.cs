﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flapper : Enemy
{

	public float upDraft = 10;

	private Vector3 spawnPos;

	public override void Spawn(Vector3 position)
	{
		base.Spawn(position);
		spawnPos = position;
		m_rigidbody.velocity = Vector3.up * upDraft;
	}

	// Update is called once per frame
	void FixedUpdate () {
		if (transform.position.y < spawnPos.y && m_rigidbody.velocity.y < 0)
		{
			m_rigidbody.velocity = new Vector3(m_rigidbody.velocity.x, upDraft);
		}
	}
}
