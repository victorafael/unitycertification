﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jumper : GroundEnemy
{
    public float jumpForce;
    void FixedUpdate()
    {
        CheckGround();
        if (isGrounded && m_rigidbody.velocity.y <= 0)
        {
            m_rigidbody.velocity = Vector2.up * jumpForce;
        }
    }
}
