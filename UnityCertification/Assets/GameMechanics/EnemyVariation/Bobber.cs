﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bobber : Enemy
{
    public float movementMagnitude;
    public float moveSpeed;
    private float pos;

    private Vector3 spawnPos;

    void FixedUpdate()
    {
        pos += moveSpeed * Time.fixedDeltaTime;
        transform.position = spawnPos + Vector3.up * (Mathf.PingPong(pos, movementMagnitude * 2) - movementMagnitude);

        if (pos > movementMagnitude * 4)
            pos -= movementMagnitude * 4;
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawLine(transform.position + Vector3.up * movementMagnitude, transform.position + Vector3.down * movementMagnitude);
    }

    public override void Spawn(Vector3 position)
    {
        base.Spawn(position);
        pos = Random.Range(0, movementMagnitude * 2);
        spawnPos = transform.position;
    }
}
