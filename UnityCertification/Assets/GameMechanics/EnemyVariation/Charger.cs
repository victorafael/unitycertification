﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Charger : Walker
{

	public LayerMask playerLayerMask;

	public float playerCheckDistance = 4;
	public float chargeSpeed = 3;

	protected override void FixedUpdate()
	{
		if (direction == 0) direction = 1;
		Debug.DrawLine(transform.position, transform.position + Vector3.right * direction * playerCheckDistance, Color.green);
		if (Physics2D.Raycast(transform.position, Vector2.right * direction, playerCheckDistance, playerLayerMask).collider !=
		    null)
		{
			m_rigidbody.velocity = new Vector3(chargeSpeed * direction, m_rigidbody.velocity.y);
		}
		else
		{
			base.FixedUpdate();
		}
	}
}
