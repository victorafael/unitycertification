﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Orbiter : Enemy
{
	public float radius;

	public float angularSpeed;
	private Vector3 spawnPos;
	private float angle;
	public Vector3 angleMagnitude;
	
	public override void Spawn(Vector3 position)
	{
		base.Spawn(position);
		spawnPos = position;

		angle = Random.value * 360;
	}

	// Update is called once per frame
	void FixedUpdate ()
	{
		angle += Time.fixedDeltaTime * angularSpeed;
		transform.position = spawnPos + Quaternion.Euler(angleMagnitude * angle) * (Vector3.right * radius);
		if (angle > 360) angle -= 360;
	}

	private void OnDrawGizmos()
	{
		Gizmos.DrawWireSphere(transform.position, radius);
	}
}
