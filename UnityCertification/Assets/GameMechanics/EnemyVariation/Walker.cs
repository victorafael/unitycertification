﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Walker : GroundEnemy
{
    protected int direction;
    public float moveSpeed = 1;
    
    protected virtual void FixedUpdate()
    {
        CheckGround();

        m_rigidbody.velocity = new Vector3(moveSpeed * direction, m_rigidbody.velocity.y);
        if(isGrounded){
            Vector3 rayPos = groundLevel.position + new Vector3(raycastOffset.x * direction, raycastOffset.y);
            Vector3 dir = ((groundLevel.position + new Vector3(wallTestOffset.x * direction, wallTestOffset.y)) - rayPos).normalized * testDistance;
            
            var hit = Physics2D.Raycast(rayPos, dir, 1.3f, groundLayer);
            if (hit.collider != null)
            {
                if (hit.point.y > groundLevel.position.y)
                {
                    //Wall found
                    direction *= -1;
                }
            }
            else
            {
                //HoleFound
                direction *= -1;
            }
        }
    }

    private void OnDrawGizmos()
    {
        if(groundLevel != null && !Application.isPlaying){
            Gizmos.color = Color.green;
            Vector3 rayPos = groundLevel.position + (Vector3)raycastOffset;
            Vector3 dir = ((groundLevel.position + (Vector3)wallTestOffset) - rayPos).normalized * testDistance;
            Gizmos.DrawLine(rayPos, rayPos + dir );
            Gizmos.DrawLine(groundLevel.position + Vector3.left, groundLevel.position + Vector3.right);
			
            Gizmos.DrawWireSphere(groundLevel.position, 0.1f);
        }
    }
    
    
    public override void Spawn(Vector3 position)
    {
        base.Spawn(position);
        direction = Mathf.RoundToInt(Mathf.Sign(Random.value - 0.5f));
    }
}
