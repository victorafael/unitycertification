﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundEnemy : Enemy {
	

	public Transform groundLevel;

	protected bool isGrounded;
	public LayerMask groundLayer;
	protected const float groundCheckRadius = 0.1f;
	

	[Header("RaycastWall")] 
	public Vector2 raycastOffset;
	public Vector2 wallTestOffset;
	public float testDistance;
	
	
	protected Collider2D[] lastGroundCheck;

	protected virtual void CheckGround()
	{
		
		lastGroundCheck = Physics2D.OverlapCircleAll(groundLevel.position, groundCheckRadius, groundLayer);
		isGrounded = false;
		for(int i = 0; i < lastGroundCheck.Length; i++){
			if(!lastGroundCheck[i].isTrigger)
			{
				isGrounded = true;
				break;
			}
		}
	}
	
	
	
    
    

	private void OnDrawGizmos()
	{
		if(groundLevel != null && !Application.isPlaying){
			Gizmos.color = Color.green;
			Vector3 rayPos = groundLevel.position + (Vector3)raycastOffset;
			Vector3 dir = ((groundLevel.position + (Vector3)wallTestOffset) - rayPos).normalized * testDistance;
			Gizmos.DrawLine(rayPos, rayPos + dir );
			Gizmos.DrawLine(groundLevel.position + Vector3.left, groundLevel.position + Vector3.right);
			
			Gizmos.DrawWireSphere(groundLevel.position, 0.1f);
		}
	}
}
