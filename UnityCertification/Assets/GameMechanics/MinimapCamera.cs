﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinimapCamera : MonoBehaviour {
	public Transform target;
	private float _minX;
    private float _startY;

	private Vector3 position;

	void Start(){
		position = transform.position;
        _startY = target.position.y;
	}

	void Update(){
		position.x = Mathf.Max(position.x, target.position.x);

        float offset = target.position.y;
        if(offset > 0)
        {
            offset /= 4;
            position.y = offset;
        }

		transform.position = position;
	}
}
