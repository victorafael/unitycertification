﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LocalizationManager : MonoBehaviour {
    private static LocalizationManager instance;

    public LocalizationData[] languages;

    public int debugLanguage = 0;

    private void Awake()
    {
        instance = this;
        foreach (var l in languages)
            l.Load();
    }

    public static string Get(string key)
    {
        if (instance == null || instance.languages == null)
            return "-- No Language --";
        return instance.languages[instance.debugLanguage][key];
    }
}
