﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class LocalizationData : ScriptableObject {
    public string[] keys;
    [Multiline]
    public string[] values;

    private Dictionary<string, string> entries;

    public void Load()
    {
        if(keys.Length != values.Length)
        {
            Debug.LogError("Invalid entries in " + name, this);
        }
        entries = new Dictionary<string, string>();
        for(int i = 0; i < keys.Length; i++)
        {
            entries.Add(keys[i], values[i]);
        }
    }

    public string this[string key]
    {
        get
        {
            return entries[key];
        }
    }
}
