﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
	public Text livesAndEnemies, score;

    public string livesLocalizationKey;
    public string scoreLocalizationKey;

	public PlayerCharacter character;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
	{
		int l, e, s;
		character.GetUIValues(out l, out e, out s);
		livesAndEnemies.text = string.Format(LocalizationManager.Get(livesLocalizationKey), l, e);
		score.text = string.Format(LocalizationManager.Get(scoreLocalizationKey), s);
	}
}
