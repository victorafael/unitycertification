﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
	public int health = 1;

	protected Rigidbody2D m_rigidbody;

	private SpriteRenderer m_spriteRenderer;

	private CircleCollider2D m_collider;
	
	// Use this for initialization
	protected virtual void Awake ()
	{
		m_rigidbody = GetComponent<Rigidbody2D>();
		m_spriteRenderer = GetComponent<SpriteRenderer>();
		m_collider = GetComponent<CircleCollider2D>();
	}


	public bool Hit()
	{
		health--;
		if (health <= 0)
		{
			Die();
			return true;
		}

		return false;
	}

	private void OnTriggerEnter2D(Collider2D other)
	{
		if (other.tag.Equals(GameplayConstants.TAG_KillZone))
		{
			gameObject.SetActive(false);
		}
	}

	protected void Die()
	{
		gameObject.SetActive(false);
	}

	public virtual void Spawn(Vector3 position)
	{
		transform.position = position;
	}
}
