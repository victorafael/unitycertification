﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MinimapElement : MonoBehaviour {
	public Color color;
    private RectTransform minimapElement;
    
    // Use this for initialization
	void OnEnable() {
        if(Minimap.instance == null)
        {
            Minimap.instance = FindObjectOfType<Minimap>();
        }
		if(minimapElement == null)
        {
            minimapElement = Minimap.instance.CreateElementDisplay(this);
        }
        minimapElement.gameObject.SetActive(true);
        Minimap.instance.UpdatePosition(transform, minimapElement);
	}
    void OnDisable()
    {
        if(minimapElement != null)
            minimapElement.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update () {
        if(minimapElement != null)
            Minimap.instance.UpdatePosition(transform, minimapElement);
    }
}
