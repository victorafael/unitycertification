﻿using UnityEngine;

public class PlayerCharacter : MonoBehaviour
{
    private Rigidbody2D rb;
    public Transform groundCheck;
    public float killEnemyHeight;

    private int lives = GameplayConstants.STARTING_LIVES;
    private int distanceScore = 0;
    private int enemyScore = 0;
    public float killEnemyJumpForce = 800;
    private int totalScore;
	void Start ()
    {
        rb = this.GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        distanceScore = Mathf.Max(distanceScore, (int)this.transform.position.x);
        totalScore = distanceScore * GameplayConstants.SCORE_DISTANCE_MULTIPLIER + enemyScore * GameplayConstants.SCORE_ENEMY_MULTIPLIER;
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == GameplayConstants.TAG_KillZone)
        {
            KillCharacter();
        }
    }
    private float lastHit;
    private void OnCollisionEnter2D(Collision2D other)
    {
        if(other.transform.tag.Equals(GameplayConstants.TAG_Enemy)){
            if(other.contacts.Length > 0){
                float killEnemyY = groundCheck.position.y + killEnemyHeight;
                if (other.contacts[0].point.y < killEnemyY)
                {
                    if (other.transform.GetComponent<Enemy>().Hit())
                    {
                        enemyScore++;
                    }
                    rb.velocity = new Vector2(rb.velocity.x, killEnemyJumpForce);
                    //Debug.Log("Force added from hit");
                    lastHit = Time.time;
                }
                else
                {
                   // Debug.LogWarning("Not hit from above");
                }
            }
            else
            {
             //   Debug.LogWarning("No contact with enemy o.O");
            }
        }
    }

    private void KillCharacter()
    {
        lives -= 1;

        if (lives > 0)
        {
            rb.MovePosition(rb.position + GameplayConstants.RESPAWN_HEIGHT * Vector2.up);
            rb.velocity = Vector2.zero;
        }
        else
        {
            GameOver();
        }
    }


    public void GetUIValues(out int lives, out int enemies, out int score)
    {
        lives = this.lives;
        score = totalScore;
        enemies = enemyScore;
    }
    
    private void GameOver()
    {
        rb.isKinematic = true;
        rb.velocity = Vector2.zero;
        Debug.Log("Game Over!");
    }

    private void OnDrawGizmos()
    {
        if(groundCheck == null) return;
        
        Gizmos.DrawLine(groundCheck.position + Vector3.up * killEnemyHeight - Vector3.right * 0.4f,
            groundCheck.position + Vector3.up * killEnemyHeight + Vector3.right * 0.4f);
    }
}
