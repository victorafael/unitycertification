﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Minimap : MonoBehaviour {
    public static Minimap instance;

    public Camera minimapCamera;
    private Vector2 resolution;

    private void Awake()
    {
        instance = this;
        resolution = GetComponent<RectTransform>().sizeDelta;
    }

    public RectTransform CreateElementDisplay(MinimapElement el)
    {
        GameObject g = new GameObject(el.name+" minimap");
        var rect = g.AddComponent<RectTransform>();
        g.AddComponent<Image>().color = el.color;
        rect.sizeDelta = new Vector2(10, 10);
        rect.anchorMin = rect.anchorMax = Vector3.zero;
        rect.SetParent(transform, false);
        return rect;
    }

    public void UpdatePosition(Transform element, RectTransform display)
    {
        var pos = minimapCamera.WorldToViewportPoint(element.position);
        float displaySize = 10;
        if(Mathf.Max(pos.x, pos.y) > 1)
        {
            displaySize = 2;
        }
        float halfSize = displaySize / 2;
        pos.x *= resolution.x;
        pos.y *= resolution.y;

        pos.x = Mathf.Clamp(pos.x, halfSize, resolution.x - halfSize);
        pos.y = Mathf.Clamp(pos.y, halfSize, resolution.y - halfSize);

        display.sizeDelta = new Vector2(displaySize, displaySize);
        display.anchoredPosition = pos;
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
