﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneController : MonoBehaviour
{
	public Object[] scenes;

	private int currentScene = 0;

	private int currentScoreCount = 0;

	public int changeLevelScore = 1000;

	private bool firstScene = true;

	void Start()
	{
		GameEvents.Register(GameConstants.Evt_Score, OnScore);
		SceneManager.sceneUnloaded += OnSceneUnloaded;
		SceneManager.sceneLoaded += OnSceneLoaded;
		SceneManager.LoadSceneAsync(scenes[currentScene].name, LoadSceneMode.Additive);
	}

	private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
	{
		currentScoreCount = 0;
		SceneManager.SetActiveScene(scene);

		if (!firstScene)
		{
			GameEvents.Trigger(GameConstants.Evt_LevelChanged);
		}

		firstScene = false;
	}
	private void OnSceneUnloaded(Scene scene)
	{
		currentScene++;
		if (currentScene >= scenes.Length)
			currentScene = 0;

		SceneManager.LoadSceneAsync(scenes[currentScene].name, LoadSceneMode.Additive);
	}

	void OnScore(int score)
	{
		currentScoreCount += score;
		if (currentScoreCount > changeLevelScore)
		{
			SceneManager.UnloadSceneAsync(scenes[currentScene].name);
		}
	}
}
