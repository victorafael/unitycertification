﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Video;

public class CinematicManager : MonoBehaviour
{
    public VideoPlayer videoPlayer;
    public GameObject overlayCanvas;
    public PlayableDirector playable;
    private bool playingVideo;
    private bool playingCinematic;
    private int cameraCulling;
    
    IEnumerator Start()
    {
        GameEvents.Register(GameConstants.Evt_LevelChanged, OnLevelChanged);
        videoPlayer.loopPointReached += OnVideoComplete;
        videoPlayer.Play();
        cameraCulling = videoPlayer.targetCamera.cullingMask;
        videoPlayer.targetCamera.cullingMask = 0;
        overlayCanvas.SetActive(false);
        playingVideo = true;
        yield return new WaitForSeconds(0.2f);
        GameEvents.Trigger(GameConstants.Evt_CinematicRunning, true);
        GameEvents.Trigger(GameConstants.Evt_Pause, true);
    }

    private void OnVideoComplete(VideoPlayer source)
    {
        ResetVideo();
    }

    void Update()
    {
        if(Input.GetKeyUp(KeyCode.Space)){
            if (playingVideo)
            {
                ResetVideo(); //Pausemanager already resumes game
            }else if (playingCinematic)
            {
                ResetCinematic();
            }
        }
        
        if (playingCinematic && playable.state != PlayState.Playing)
        {
            ResetCinematic();
        }
    }

    private void ResetVideo()
    {
        videoPlayer.Stop();
        videoPlayer.clip = null;
        playingVideo = false;
        GameEvents.Trigger(GameConstants.Evt_Pause, false);
        GameEvents.Trigger(GameConstants.Evt_CinematicRunning, false);
        overlayCanvas.SetActive(true);
        videoPlayer.targetCamera.cullingMask = cameraCulling; 
    }

    private void ResetCinematic()
    {
        playable.Stop();
        GameEvents.Trigger(GameConstants.Evt_Pause, false);
        GameEvents.Trigger(GameConstants.Evt_CinematicRunning, false);
    }
    
    void OnLevelChanged()
    {
        playingCinematic = true;
        playable.Play();
        
        GameEvents.Trigger(GameConstants.Evt_CinematicRunning, true);
        GameEvents.Trigger(GameConstants.Evt_Pause, true);
    }
}
