﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PostProcessing;

public class PostProcessingFX : MonoBehaviour
{
	public PostProcessingProfile profile;
	public Color angryFairyColor;

	public float angryFairyIntensity;

	public float angryFairyDecaytime;

	private float currentValue;
	// Use this for initialization
	void Start () {
		GameEvents.Register(GameConstants.Evt_AngryFairy, AngryFairy);
		currentValue = 0;
		ApplyFX(0);
	}

	void AngryFairy()
	{
		var settings = profile.vignette.settings;
		settings.color = angryFairyColor;
		settings.intensity = angryFairyIntensity;
		currentValue = angryFairyIntensity;
		profile.vignette.settings = settings;
	}

	void ApplyFX(float ammount)
	{
		var settings = profile.vignette.settings;
		settings.color = Color.Lerp(angryFairyColor, Color.black, 1 - ammount);
		settings.intensity = ammount;
		profile.vignette.settings = settings;
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyUp(KeyCode.P))
		{
			AngryFairy();
		}
		if (currentValue > 0)
		{
			currentValue = Mathf.MoveTowards(currentValue, 0, Time.deltaTime / angryFairyDecaytime);
			ApplyFX(currentValue);
		}
	}
}
