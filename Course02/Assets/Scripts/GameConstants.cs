﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameConstants
{
    public const int Layer_Enemies = 9;
    public const int Layer_Shootable = 8;


    public const string Evt_Sound = "sound";
    public const string Evt_CollectGrenade = "getgrenade";
    public const string Evt_ShootGrenade = "shootGrenade";
    public const string Evt_Score = "score";
    public const string Evt_Pause = "pause";
    public const string Evt_LevelChanged = "levelChanged";
    public const string Evt_CinematicRunning = "cinematicRunning";
    public const string Evt_AngryFairy = "AngryFairy";
}
