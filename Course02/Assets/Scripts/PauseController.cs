﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseController : MonoBehaviour
{
	private bool m_isPaused;
	private bool canPause;
	public static bool IsPaused { get; private set; }
	private float lastCinematicEvt;
	void Start()
	{
		GameEvents.Register(GameConstants.Evt_CinematicRunning, OnCinematicRunning);
	}

	void OnCinematicRunning(bool running)
	{
		canPause = !running;
		lastCinematicEvt = Time.time;
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (canPause && Input.GetKeyUp(KeyCode.Space) && Time.time - 0.1f > lastCinematicEvt)
		{
			m_isPaused = !m_isPaused;
			GameEvents.Trigger(GameConstants.Evt_Pause, m_isPaused);
		}	
	}
}
