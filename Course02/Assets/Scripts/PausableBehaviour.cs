﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PausableBehaviour : MonoBehaviour {
	public bool IsPaused { get; private set; }

	protected virtual void Start()
	{
		GameEvents.Register(GameConstants.Evt_Pause, OnPauseStatusChange);
	}

	protected virtual void OnPauseStatusChange(bool isPaused)
	{
		IsPaused = isPaused;
	}
}
