﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameEvents : MonoBehaviour
{
	private static Dictionary<string, UnityEvent> voidEvents;
	private static Dictionary<string, UnityEvent<Vector3>> vectorEvents;
	private static Dictionary<string, UnityEvent<bool>> boolEvents;
	private static Dictionary<string, UnityEvent<int>> intEvents;
	
	private class VectorEvt : UnityEvent<Vector3>{}
	private class BoolEvt : UnityEvent<bool>{}
	private class IntEvt: UnityEvent<int>{}
	// Use this for initialization
	private void Awake()
	{
		voidEvents = new Dictionary<string, UnityEvent>();
		vectorEvents = new Dictionary<string, UnityEvent<Vector3>>();
		boolEvents = new Dictionary<string, UnityEvent<bool>>();
		intEvents = new Dictionary<string, UnityEvent<int>>();
	}

	

	static void CheckVoid(string key)
	{
		if (!voidEvents.ContainsKey(key))
			voidEvents.Add(key, new UnityEvent());
	}

	static void CheckVector(string key)
	{
		if(!vectorEvents.ContainsKey(key))
			vectorEvents.Add(key, new VectorEvt());
	}

	static void CheckBool(string key)
	{
		if(!boolEvents.ContainsKey(key))
			boolEvents.Add(key, new BoolEvt());
	}

	static void CheckInt(string key)
	{
		if(!intEvents.ContainsKey(key))
			intEvents.Add(key, new IntEvt());
	}
	
	
	
	public static void Register(string key, UnityAction action)
	{
		CheckVoid(key);
		voidEvents[key].AddListener(action);
	}
	public static void Register(string key, UnityAction<Vector3> action)
	{
		CheckVector(key);
		vectorEvents[key].AddListener(action);
	}

	public static void Register(string key, UnityAction<bool> action)
	{
		CheckBool(key);
		boolEvents[key].AddListener(action);
	}

	public static void Register(string key, UnityAction<int> action)
	{
		CheckInt(key);
		intEvents[key].AddListener(action);
	}

	public static void Trigger(string key)
	{
		CheckVoid(key);
		voidEvents[key].Invoke();
	}

	public static void Trigger(string key, Vector3 pos)
	{
		CheckVector(key);
		vectorEvents[key].Invoke(pos);
	}

	public static void Trigger(string key, bool value)
	{
		CheckBool(key);
		boolEvents[key].Invoke(value);
	}

	public static void Trigger(string key, int value)
	{
		CheckInt(key);
		intEvents[key].Invoke(value);
	}
	
	public static void UnRegister(string key, UnityAction action)
	{
		CheckVoid(key);
		voidEvents[key].RemoveListener(action);
	}

	public static void UnRegister(string key, UnityAction<Vector3> action)
	{
		CheckVector(key);
		vectorEvents[key].RemoveListener(action);
	}

	public static void UnRegister(string key, UnityAction<bool> action)
	{
		CheckBool(key);
		boolEvents[key].RemoveListener(action);
	}
	public static void UnRegister(string key, UnityAction<int> action)
	{
		CheckInt(key);
		intEvents[key].RemoveListener(action);
	}
}
